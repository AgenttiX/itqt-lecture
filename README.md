# Quantum imaging and tomography
## Lecture for the course Introduction to Quantum Technologies

[![pipeline status](https://gitlab.com/AgenttiX/itqt-lecture/badges/master/pipeline.svg)](https://gitlab.com/AgenttiX/itqt-lecture/-/commits/master)

This lecture was held on 2020-02-20 by Mika Mäki as the last lecture of the [Tampere University](https://www.tuni.fi/) course [Introduction to Quantum Technologies](https://moodle.tuni.fi/course/info.php?id=4775).

Created by Mika Mäki<br>
[Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) except for the images which are subject to original copyrights of the authors
