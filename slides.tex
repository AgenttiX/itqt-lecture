\documentclass[aspectratio=169]{beamer}
\usetheme{Madrid}
\usepackage[utf8]{inputenc}

\usepackage[backend=biber]{biblatex}
\addbibresource{references.bib}

% This breaks the list styles
% \usepackage{enumitem}
\usepackage{qrcode}
\usepackage{textcomp}

\title{Quantum imaging and tomography}
\author{Mika Mäki}
\centering
\date{2020-02-20}


\begin{document}
\maketitle

\begin{frame}{Content}
\begin{columns}
\begin{column}{0.68\textwidth}
\begin{itemize}
\item Introduction
\item Quantum theory of direct and homodyne detection
\item Quantum imaging techniques
\begin{itemize}
    \item Sub-shot-noise imaging
    \item Super-resolution imaging
    \item Ghost imaging
    \item Quantum imaging with undetected photons
    \item Single-pixel camera
\end{itemize}
\item Quantum state tomography
\end{itemize}
\end{column}

\begin{column}{0.28\textwidth}
\centering
You can download the slides and the report here: \\
\qrcode{https://gitlab.com/AgenttiX/itqt-lecture/-/jobs/artifacts/master/download?job=compile_pdf}
\end{column}
\end{columns}
\end{frame}


\begin{frame}{Quantum imaging}
% \framesubtitle{Subtitle}
\begin{itemize}
\item An emerging field
\item Enables imaging systems beyond classical limitations
\begin{itemize}
	\item Noise
	\item Resolution
\end{itemize}
\item Imaging without measuring properties of the photons that interact with the object
\end{itemize}

% TODO Add the Schrödinger's cat here
\end{frame}


\begin{frame}{Quantum theory of direct detection \cite{loudon_quantum_2000}}
\begin{columns}
\begin{column}{0.68\textwidth}
\begin{itemize}
\item Simple photocounting
\item Measures only intensity, not phase
\item Number of photons arriving on the detector
\begin{equation}
\hat{M}(t, T) = \int_t^{t+T} dt' \hat{f}(t') = \int_t^{t+T} dt' \hat{a}^\dagger (t') \hat{a}(t)
\end{equation}
\end{itemize}
\end{column}

\begin{column}{0.28\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{fig/phototubes.jpg}
\caption{Phototubes \cite{phototubes}}
\label{fig:phototubes}
\end{figure}
\end{column}
\end{columns}
\end{frame}


\begin{frame}
\begin{columns}
\begin{column}{0.48\textwidth}
\begin{itemize}
\item For phototubes $\eta \in [0.1, 0.4]$
\item Can be modeled as a beam splitter
\begin{equation}
r = i \sqrt{1 - \eta} \quad \text{and} \quad t = \sqrt{\eta}
\end{equation}
\begin{equation}
\hat{d}(t) = \sqrt{\eta} \hat{a}(t) + i \sqrt{1 - \eta} \hat{v}(t)
\end{equation}
\item Photocount operator
\begin{equation}
    \hat{M}_D(t, T) = \int_t^{t+T} dt' \hat{d}^\dagger (t') \hat{d} (t')
\end{equation}
\item Mean photocount
\begin{equation}
    \langle m \rangle
    = \langle \hat{M}_D (t, T) \rangle
    = \eta \langle \hat{M} (t, T) \rangle
\end{equation}
\end{itemize}
\end{column}

\begin{column}{0.48\textwidth}
\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{fig/imperfect_detector.png}
\caption{An inefficient photodetector \cite[p. 272]{loudon_quantum_2000}}
\label{fig:imperfect_detector}
\end{figure}
\end{column}
\end{columns}
\end{frame}


\begin{frame}
\begin{itemize}
\item Second factorial moment of the photocount numbers
\begin{align}
    \langle m(m-1) \rangle
    &= \Big\langle \hat{M}_D (t, T) \left( \hat{M}_D(t, T)- 1 \right) \Big\rangle \label{eq:second_moment} \\
    &= \Big\langle : \left( \hat{M}_D(t, T) \right)^2 : \Big\rangle \label{eq:second_moment_colon} \\
    &= \int_t^{t+T} dt' \int_t^{t+T} dt'' \langle \hat{d}^\dagger (t') \hat{d}^\dagger (t'') \hat{d} (t'') \hat{d} (t') \rangle \label{eq:second_moment_long} \\
    &= \eta^2 \Big\langle : \left( \hat{M}(t, T) \right)^2 : \Big\rangle \label{eq:second_moment_eta} \\
    &= \eta^2 \Big\langle \hat{M} (t, T) \left( \hat{M} (t, T) - 1 \right) \Big\rangle
\end{align}
\item Variance
\begin{equation}
    (\Delta m)^2
    = \eta^2 \Big\langle \left( \Delta \hat{M} (t, T) \right)^2 \Big\rangle
    + \eta (1 - \eta) \big\langle \hat{M} (t, T) \big\rangle
\end{equation}
\end{itemize}
\end{frame}


\begin{frame}
\begin{itemize}
\item Degree of second-order coherence
\begin{equation}
g_D^{(2)}(0)
= \frac{\langle m(m-1) \rangle}{\langle m \rangle^2}
= \frac{\Big\langle \hat{M}(t,T) \big( \hat{M}(t,T) - 1 \big) \Big\rangle}{\Big\langle \hat{M}(t,T) \Big\rangle^2}.
\end{equation}
\item Non-classical range of values
\begin{equation}
1 - \frac{1}{\Big\langle \hat{M}(t,T) \Big\rangle} \leq g_D^{(2)}(0) < 1.
\end{equation}
\item Fully quantum mechanical photocount distribution
\begin{equation}
P_m (t, T) = \Big\langle : \frac{\left( \eta \hat{M} (t, T) \right)^m}{m!} e^{-\eta \hat{M} (t, T)} : \Big\rangle
\end{equation}
\item For stationary coherent light
\begin{equation}
\langle m \rangle = \eta FT
\end{equation}
\begin{equation}
P_m (T) = \frac{ \langle m \rangle^m}{m!} e^{-\langle m \rangle}
\end{equation}

% This did not fit on the slide
% \iffalse
% \item For chaotic light
% \begin{equation}
% \hat{M}(t,T)
% = \hat{f}(t) T
% = \hat{a}^\dagger (t) \hat{a} (t) T.
% \end{equation}
% \fi
\end{itemize}
\end{frame}


\begin{frame}{Homodyne detection \cite{loudon_quantum_2000}}
\begin{itemize}
\item Homodyne = source and reference have approximately the same frequency
\item Sensitive to phase
\item Applications
\begin{itemize}
\item Distinguishing between various kinds of light
\item For study of squeezed light (nonclassical effects, phase dependent)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\begin{columns}
\begin{column}{0.58\textwidth}
\begin{itemize}
\item Balanced homodyne detection
\begin{itemize}
    \item Two detectors
    \item Measuring difference
\end{itemize}
\item Beam splitter
\begin{equation}
|r| = |t| = \frac{1}{\sqrt{2}} \quad \text{and} \quad \phi_r - \phi_t = \frac{\pi}{2}
\end{equation}
\begin{align}
& \hat{M}_{-}(t, T) \\
&= \int_t^{t+T} dt' \Big( \hat{f}_3 (t') - \hat{f}_4 (t') \Big) \\
&= \int_t^{t+T} dt' \Big( \hat{a}_3^\dagger (t') \hat{a}_3 (t') - \hat{a}_4^\dagger (t') \hat{a}_4 (t') \Big) \\
&= i \int_t^{t+T} dt' \Big( \hat{a}^\dagger (t') \hat{a}_L (t') - \hat{a}_L^\dagger (t') \hat{a} (t') \Big)
\end{align}
\end{itemize}
\end{column}

\begin{column}{0.38\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{fig/balanced_homodyne.png}
\caption{A balanced homodyne detector \cite[p. 279]{loudon_quantum_2000}}
\label{fig:balanced_homodyne}
\end{figure}
\end{column}
\end{columns}
\end{frame}


\begin{frame}
\begin{itemize}
\item Non-ideal photodetectors $\rightarrow$ the quantity measured is
\begin{equation}
    \hat{M}_H (t,T) = \int_t^{t+T} dt' \Big( \hat{d}_3^\dagger (t') \hat{d}_3 (t') - \hat{d}_4^\dagger (t') \hat{d}_4 (t') \Big)
\end{equation}
\item Expectation value
\begin{align}
    \langle m_{-} \rangle
    &= \big\langle \hat{M}_H (t,T) \big\rangle \\
    &= \eta \big\langle \hat{M}_{-} (t,T) \big\rangle \\
    &= i \eta \int_t^{t+T} dt' \Big\langle \hat{a}^\dagger (t') \hat{a}_L (t') - \hat{a}_L^\dagger (t') \hat{a} (t') \Big\rangle
\end{align}
\item Variance
\begin{equation}
    (\Delta m_{-})^2
    = \eta^2 \Big\langle \big( \Delta \hat{M}_{-} (t,T) \big)^2 \Big\rangle
    + \eta (1-\eta) \int_t^{t+T} dt' \Big\langle \hat{a}_L^\dagger \hat{a}_L (t') + \hat{a}^\dagger (t') \hat{a} (t') \Big\rangle
\end{equation}
\end{itemize}
\end{frame}


\begin{frame}{Homodyne detection}
\framesubtitle{For single-mode coherent beams}
\begin{itemize}
\item Quite a few equations! Please bear with me, as we are looking for the effects of squeezed light.
\item Complex amplitude for a \textbf{single-mode coherent beam} (local oscillator)
\begin{equation}
\alpha_L (t) = F_L^{\frac{1}{2}} e^{-i \omega_L t + i \theta_L }
\end{equation}
\item With the eigenvalue properties of the operator $\hat{a}_L$
\begin{equation}
\langle m_{-} \rangle = i \eta \sqrt{F_L} \int_t^{t+T} dt' \Big\langle \hat{a}^\dagger (t') e^{-i \omega_L t' + i \theta_L} - \hat{a}(t') e^{i \omega_L t' - i \theta_L} \Big\rangle
\end{equation}
\item Let's define the \textbf{homodyne electric-field operator} (where $\chi = \theta_L + \frac{\pi}{2}$)
\begin{equation}
\hat{E}_H (\chi, t, T) = \frac{1}{2} T^{-\frac{1}{2}} \int_t^{t+T} dt' \left( \hat{a}^\dagger (t') e^{-i \omega_L t' + i \chi} + \hat{a}(t') e^{i \omega_L t' - i \chi} \right).
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}
\begin{itemize}
\item This simplifies the previously defined mean difference photocount to
\begin{equation}
\langle m_{-} \rangle = 2 \eta \sqrt{F_L T} \left\langle \hat{E}_H (\chi, t, T) \right\rangle.
\end{equation}
\item \textbf{When the local oscillator flux is much greater than the signal flux} we can approximate the variance
\begin{equation}
(\Delta m_{-})^2 = \eta F_L T \left( 4 \eta \left\langle \left( \Delta \hat{E}_H (\chi, t, T) \right)^2 \right\rangle + 1 - \eta \right).
\end{equation}
\item Assuming the signal field is also on a single-mode coherent state
\begin{equation}
\alpha (t) = \sqrt{F} e^{-i \omega_L t + i \theta}.
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}
\begin{itemize}
\item Signal field (by substituting the coherent amplitude to the homodyne electric-field operator)
\begin{equation}
\mathcal{S} = \left\langle \hat{E}_H (\chi, t, T) \right\rangle = \sqrt{FT} \cos (\chi - \theta).
\end{equation}
\item Variance, aka. noise
\begin{equation}
\mathcal{N} = \left\langle \left( \Delta \hat{E}_H (\chi, t, T) \right) \right\rangle = \frac{1}{4}
\end{equation}
\item Signal-to-noise ratio
\begin{equation}
SNR = \frac{\mathcal{S}}{\mathcal{N}} = 4FT \cos^2 (\chi - \theta).
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}
\begin{itemize}
\item Let's finally discuss squeezed light!
\item Properties of squeezed ligth are in the frequency domain $\rightarrow$ we have to Fourier transform our time-dependent operators
\item Due to mathematical properties of the operators
\begin{equation}
\left\langle \hat{E}_H (\chi, t, T) \right\rangle = 0.
\quad \land \quad
\langle m_{-} \rangle = 0
\end{equation}
\item But variance depends on the squeezing! If $T$ is long enough
\begin{equation}
\left\langle \left( \Delta \hat{E}_H (\chi, t, T) \right)^2 \right\rangle
= \frac{1}{4} \left( e^{2s(\omega_L)} \sin^2 \left( \chi - \frac{1}{2} v(\omega_L) \right)
+ e^{-2s(\omega_L)} \cos^2 \left(\chi - \frac{1}{2} v(\omega_L) \right) \right)
\end{equation}
\item Signal field squeezing happens when
\begin{equation}
0 \leq \left\langle \left( \Delta \hat{E}_H (\chi, t, T) \right)^2 \right\rangle < \frac{1}{4}.
\end{equation}
\item The squeezing results in
\begin{equation}
0 \leq (\Delta m_{-})^2 < \eta F_L T.
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}{Imaging techniques: image intensifiers}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{fig/image_intensifier.png}
\caption{An image intensifier \cite{image_intensifier}}
\label{fig:image_intensifier}
\end{figure}
\end{frame}


\begin{frame}{Sub-shot-noise imaging \cite{imaging_states}}
\begin{center}
\begin{figure}[ht!]
\centering
\includegraphics[width=0.9\textwidth]{fig/sub-shot-noise.png}
\caption{a) A measurement setup for ghost imaging, b) right: direct images of the object, left: difference images \cite{imaging_states}}
\label{fig:sub-shot-noise}
\end{figure}
\end{center}
\end{frame}


\begin{frame}{Quantum correlations \cite{imaging_states}}
\begin{figure}[ht!]
\centering
\includegraphics[width=0.7\textwidth]{fig/quantum_correlations.png}
\caption{Generation and detection of quantum correlations in spontaneous parametric down-conversion \cite{imaging_states}}
\label{fig:quantum_correlations}
\end{figure}
\end{frame}


\begin{frame}{Super-resolution imaging \cite{imaging_states}}
\begin{itemize}
\item Quantum correlations between $N$ photons
\begin{itemize}
	\item Resolution improvement: $\frac{1}{\sqrt{N}}$ scaling
	\item Single-photon emitters $\rightarrow$ photon anti-bunching
	\item Capture stack of images $\rightarrow$ compute the positions of emitters
	\item Can be done with non-fluorescent objects as well with tricks
\end{itemize}
\item Quantum lithography
\begin{itemize}
	\item Resolution improvement: $\frac{1}{N}$ (Heisenberg scaling)
	\item Similar to the Hong-Ou-Mandel experiment (in which two photons exit from the beam splitter together)
	\item Interference in multiphoton absorption
\end{itemize}
\item Difficult to find multiphoton absorptive detectors $\rightarrow$ alternative versions are being developed
\end{itemize}
\end{frame}

\begin{frame}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{fig/quantum_lithography_a.png}
\caption{A quantum lithography setup \cites{imaging_states}}
\label{fig:quantum_lithography_a}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[ht!]
\centering
\includegraphics[width=0.7\textwidth]{fig/quantum_lithography_b.png}
\caption{The black graph is a single-photon count rate for a coherent-state light beam, the red graphs are single-photon count rates for a 2002 (NOON) state, and the green graph is the two-photon count rate for a 2002 state. \cites{imaging_states}{shin_quantum_2011}}
\label{fig:quantum_lithography_b}
\end{figure}
\end{frame}

\begin{frame}{Ghost imaging \cite{imaging_states}}
\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{fig/ghost_imaging.png}
% \caption{A setup for ghost imaging \cite{imaging_states}}
\label{fig:ghost_imaging}
\end{figure}
\end{frame}


\begin{frame}{Quantum imaging with undetected photons \cite{imaging_states}}
\begin{figure}[ht!]
\centering
\includegraphics[width=0.53\textwidth]{fig/undetected_imaging.png}
% \caption{A setup for quantum imaging with undetected photons \cite{imaging_states}}
\label{fig:undetected_imaging}
\end{figure}
\end{frame}


\begin{frame}{Single-pixel camera}
\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{fig/single-pixel_camera.png}
\caption{A single-pixel camera \cite{imaging_information}}
\label{fig:single-pixel}
\end{figure}
\end{frame}


\begin{frame}{Quantum state tomography}
\begin{itemize}
\item Extraction of information from quantum states
\begin{itemize}
\item Ideally complete characterization
\end{itemize}
\item Requires a stream of identical qubits
\item Due to measurement errors, usually gives unphysical results
\begin{itemize}
\item But the correct state can be found with the maximum likelihood method
\end{itemize}
\item Scaling to multiple qubits
\begin{itemize}
\item The qubits have $2^n$ basis states
\item $2n$ concidence detectors
\item Simplest setups: exponential amount of measurements required ($3^n$)
\item Commonly used setups: $n^2$ measurements
\end{itemize}
\item Optimization algorithms similar to those of the single-pixel camera can be used to reduce the amount of measurements required
\end{itemize}
\end{frame}

\begin{frame}
\begin{figure}
\centering
\includegraphics[width=0.68\textwidth]{fig/tomography_measurements.png}
\caption{Illustration of single-qubit quantum tomography. a) A sequence of measurements along the right-circular, diagonal and horizontal axes. b) A sequence of measurements on elliptical light rotated 30\textdegree \ from H towards R, 22.5\textdegree \ linear, and horizontal \cite{paris_4_2004}}
\label{fig:tomography_measurements}
\end{figure}
\end{frame}

\begin{frame}
\centering
\vspace*{\fill}
{\Huge Thank you! \\}
\bigskip
{\Large Questions?}
\vspace*{\fill}
\end{frame}

\begin{frame}[allowframebreaks]
\frametitle{References}
% For making references appear without citations
% \nocite{*}
\printbibliography[heading=bibintoc]
\end{frame}


\iffalse
\begin{frame}{Thank you}
Sources
\begin{itemize}
    \item The Quantum Theory of Light, 3rd ed., Rodney Loudon, 2000
    \item Imaging with quantum states of light, P-A. Moreau et al., 2019, \href{https://doi.org/10.1038/s42254-019-0056-0}{doi:10.1038/s42254-019-0056-0}
    \item Quantum Imaging and Information, O.S. Magana-Loaiza, 2019, \href{https://doi.org/10.1088/1361-6633/ab5005}{doi:10.1088/1361-6633/ab5005}
\end{itemize}
\end{frame}
\fi


\end{document}
